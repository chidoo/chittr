# Chittr App
This repo serves as the main entry point for this microblogging mobile application. This application is linked to an API which allows the functionality of creating an account, logging into an existing account and being able to view and post chits.


## Coding Standards - Frontend JS

The Chittr App code adheres to the following https://standardjs.com/ rules.

## How to run Chittr App

1) Start the server is up and running

2) Open android studio and run an emulator  (**This app was developed using a Pixel 2 API 28**)

3) Open your command line and navigate to the Chittr App and use the following command to install the application. If it fails on the first try please re-try
```
npm install
```
4) Once installed then you can now run the app using the following command this should open a metro bundler and lauch the application withing the emulator
```
npx react-native run-android
```

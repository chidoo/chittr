/* global fetch */

import 'react-native-gesture-handler'
import React, { Component, useState, useEffect } from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import {
  createDrawerNavigator,
  DrawerItem,
  DrawerContentScrollView
} from '@react-navigation/drawer'
import AsyncStorage from '@react-native-community/async-storage'
import { Drawer, Portal, FAB, Provider } from 'react-native-paper'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

// Screens
import AllChitsFeed from './screens/AllChits'
import SignUp from './screens/SignUp'
import Login from './screens/LoginScreen'
import SingleChit from './screens/SingleChitScreen'
import SearchUsers from './screens/Search'
import UserDetails from './screens/SingleUser'
import PostChit from './screens/PostChit'
import SavedChits from './screens/DraftChits'
import Profile from './screens/UserProfile'
import CaptureImage from './screens/ImageCapture'
import { styles } from './styles/Styles'

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()
const Draw = createDrawerNavigator()

function HomeStack () {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name='AllChits'
        component={AllChitsFeed}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='SingleChit'
        component={SingleChit}
        options={{
          title: 'Chit',
          headerTitleAlign: 'center'
        }}
      />
      <Stack.Screen
        name='SingleUser'
        component={UserDetails}
        options={{
          headerTitleAlign: 'center'
        }}
      />
      <Stack.Screen
        name='PostChit'
        component={PostChit}
        options={{ title: 'Post a chit', headerTitleAlign: 'center' }}
      />
      <Stack.Screen
        name='Drafts'
        component={SavedChits}
        options={{ title: 'Saved Drafts', headerTitleAlign: 'center' }}
      />
      <Stack.Screen
        name='Profile'
        component={Profile}
        options={{ headerTitleAlign: 'center' }}
      />
      <Stack.Screen
        name='Capture'
        component={CaptureImage}
        options={{
          title: 'Take a picture',
          headerTitleAlign: 'center'
        }}
      />
      <Stack.Screen
        name='Signup'
        component={SignUp}
        options={{
          title: 'Create Account',
          headerTitleAlign: 'center'
        }}
      />
      <Stack.Screen
        name='Login'
        component={Login}
        options={{
          headerTitleAlign: 'center'
        }}
      />
    </Stack.Navigator>
  )
}

function BottomNav ({ navigation }) {
  const [token, setToken] = useState('')

  AsyncStorage.getItem('userToken').then(value => setToken(value))
  return (
    <>
      <Provider>
        <Tab.Navigator
          initialRouteName='Home'
          tabBarOptions={{
            activeTintColor: '#FF00FF'
          }}
        >
          <Tab.Screen
            onPress={() => {
              navigation.navigate('AllChits')
            }}
            name='Home'
            component={HomeStack}
            options={{
              tabBarLabel: 'HOME',
              tabBarIcon: ({ focused, color, size }) => (
                <MaterialCommunityIcons name='home-outline' size={35} color={color} />
              )
            }}
          />
          <Tab.Screen
            name='Search' component={SearchUsers}
            options={{
              tabBarLabel: 'SEARCH',
              tabBarIcon: ({ focused, color, size }) => (
                <MaterialCommunityIcons name='account-search-outline' color={color} size={35} />
              )
            }}

          />
        </Tab.Navigator>
        {token != null && (
          <Portal>
            <FAB
              onPress={() => {
                navigation.navigate('PostChit', token)
              }}
              color='#FF00FF'
              icon='feather'
              style={{
                position: 'absolute',
                bottom: 100,
                right: 18
              }}
            />
          </Portal>
        )}
      </Provider>
    </>
  )
}

async function logout (token, navigation) {
  try {
    await fetch('http://10.0.2.2:3333/api/v0.0.5/logout', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'X-Authorization': token
      }
    })
    await AsyncStorage.clear()
    console.log('LOGGED OUT')
    navigation.toggleDrawer()
    navigation.navigate('AllChits')
  } catch (error) {
    console.error('ERROR', error)
  }
}

function DrawerContent (props) {
  const [userToken, setUserToken] = useState('')
  useEffect(() => {
    const getData = async () => {
      try {
        setUserToken(await AsyncStorage.getItem('userToken'))
      } catch (e) {
        // Restoring token failed
        console.error('ERROR RETRIEVING TOKEN' + e)
      }
    }
    getData()
  })

  return (
    <DrawerContentScrollView>
      <Drawer.Section
        style={styles.drawer}
      >
        {userToken == null ? (
          <>
            {/* Not signed in */}
            <DrawerItem
              label='Login'
              onPress={() => {
                props.navigation.navigate('Login', props.navigation)
              }}
              icon={({ focused, color, size }) => (
                <MaterialCommunityIcons name='login' color={color} size={35} />)}
            />
            <DrawerItem
              label='Create Account'
              onPress={() => {
                props.navigation.navigate('Signup', props.navigation)
              }}
              icon={({ focused, color, size }) => (
                <MaterialCommunityIcons name='account-plus-outline' color={color} size={35} />)}
            />
          </>
        ) : (
          <>
            {/* Signed in */}
            <DrawerItem
              label='Profile'
              onPress={() => props.navigation.navigate('Profile')}
              icon={({ focused, color, size }) => (
                <MaterialCommunityIcons name='account-outline' color={color} size={35} />)}
            />
            <DrawerItem
              label='Saved Drafts'
              onPress={() => props.navigation.navigate('Drafts', userToken)}
              icon={({ focused, color, size }) => (
                <MaterialCommunityIcons name='file' color={color} size={35} />)}
            />
            <DrawerItem
              label='Logout'
              onPress={() => logout(userToken, props.navigation)}
              icon={({ focused, color, size }) => (
                <MaterialCommunityIcons name='logout' color={color} size={35} />)}
            />
          </>
        )}
      </Drawer.Section>
    </DrawerContentScrollView>
  )
}
export default class DrawerScreen extends Component {
  constructor (props) {
    super(props)
    this.state = {
      userToken: null
    }
  }

  render () {
    return (
      <NavigationContainer>
        <Draw.Navigator drawerContent={props => <DrawerContent {...props} />}>
          <Draw.Screen name='Tab' component={BottomNav} />
        </Draw.Navigator>
      </NavigationContainer>
    )
  }
}

import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  chit: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 5
  },
  drawer: {
    marginTop: 16
  },
  user: {
    fontWeight: 'bold',
    fontSize: 19,
  },
  single_chit: {
    backgroundColor: '#fff',
    padding: 8
  },
  chit_content: {
    marginTop: 10,
    marginBottom: 10,
    fontSize: 17
  },
  user_content: { fontSize: 17, fontWeight: 'bold', marginTop: 10 },
  rowAlign: {
    flexDirection: 'row'
  },
  profileButton: {
    alignItems: 'center',
    alignContent: 'center',
    marginTop: 5,
    padding: 10,
    borderRadius: 5,
    backgroundColor: '#7FFF00',
    marginLeft: 50,
    marginRight: 50
  },
  textInput: {
    borderBottomColor: 'blue',
    borderRightColor: '#fff',
    borderLeftColor: '#fff',
    borderTopColor: '#fff',
    borderWidth: 1,
    height: 50,
    fontSize: 16
  },
  profileImage: {
    width: 100,
    height: 100,
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 50
  },
  placholderColour: {
    color: '#D3D3D3'
  },
  feedImage: {
    height: 200,
    width: 200,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  singleImage: {
    marginTop: 10,
    marginBottom: 10,
    height: 350,
    width: 350,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  greyText: {
    color: '#808080'
  },
  userImage: {
    width: 70,
    height: 70,
    marginRight: 7,
    borderRadius: 50

  },
  button: {
    alignItems: 'center',
    alignContent: 'center',
    marginTop: 5,
    padding: 10,
    borderRadius: 5,
    marginLeft: 50,
    marginRight: 50
  },
  deleteButton: {
    alignItems: 'center',
    alignContent: 'center',
    marginTop: 5,
    padding: 10,
    borderRadius: 5,
    backgroundColor: 'red',
    marginLeft: 50,
    marginRight: 50
  },
  postChit:{
    marginLeft: 50,
    marginRight: 50,
    backgroundColor: '#00BFFF',
    alignItems: 'center',
    alignContent: 'center',
    marginTop: 5,
    padding: 10,
    borderRadius: 5
  },
  container: { flex: 1, flexDirection: 'column' },
  preview: { flex: 1, justifyContent: 'flex-end', alignItems: 'center' },
  capture: {
    flex: 0,
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
    marginTop: 6
  },
  profileInputBoxes: {
    maxWidth: 200,
    minWidth: 200
  }
})

export { styles }

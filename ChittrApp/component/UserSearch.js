import React, { Component } from 'react'
import { Text, Image } from 'react-native'
import PropTypes from 'prop-types'
import { styles } from '../styles/Styles'
import { TouchableOpacity } from 'react-native-gesture-handler'

export default class UserList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      data: this.props.item
    }
  }

  render () {
    return (
      <TouchableOpacity
        onPress={() => {
          this.props.navigation.navigate('SingleUser', this.state.data)
        }}
      >
        <Text
          style={styles.user}
        >
          {`${this.state.data.given_name} ${this.state.data.family_name}`}
        </Text>
        <Text style={styles.greyText}>{this.state.data.email}</Text>
      </TouchableOpacity>
    )
  }
}

UserList.propTypes = { item: PropTypes.object.isRequired }

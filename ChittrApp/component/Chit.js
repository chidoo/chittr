import React, { Component } from 'react'
import { Text, View, Image, ActivityIndicator } from 'react-native'
import { styles } from '../styles/Styles'
import { TouchableOpacity } from 'react-native-gesture-handler'


export default class Chit extends Component {
  _isMounted = false;
  constructor (props) {
    super(props)
    this.state = {
      data: this.props.item,
      isLoading:true
    }
  }

  getUserProfilePicture (userId) { // User's profile pictures
    return fetch(`http://10.0.2.2:3333/api/v0.0.5/user/${userId}/photo`)
      .then(response => {
        this.setState({
          img: response.url,
          statusCode: response.status,
          isLoading: false
        })
      })
      .catch(error => {
        console.error('ERROR OCCURED: ', error)
      })
  }

  getChitImage (chitId) { // Retrieve images for chits
    return fetch(`http://10.0.2.2:3333/api/v0.0.5/chits/${chitId}/photo`)
      .then(response => {
        this.setState({
          image: response.url,
          status: response.status,
          isLoading: false
        })
      })
      .catch(error => {
        console.error('ERROR OCCURED: ', error)
      })
  }

  timestamp (time) {
    const date = new Date(time)
    const postDate = `${date.getHours()}:${date.getMinutes()}  ${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`
    return postDate.toString()
  }

  componentDidMount () {
    this._isMounted = true;
    if (this.props.data) {
      this.getChitImage(this.props.data.chit_id)
    } else {
      this.getUserProfilePicture(this.props.item.user.user_id)
      this.getChitImage(this.props.item.chit_id)
    }
  }
  componentWillUnmount() {
    this._isMounted = false;
  }

  render () {
    if (this.state.isLoading) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size='large' color='#0c9' />
        </View>
      )
    }
    return (
      <View>
        {this.props.data ? (
          <>
            <View>
              <Text>
                {this.props.data.chit_content}
              </Text>
              {this.state.status == 200 && (
                <Image
                  style={styles.feedImage}
                  source={{ uri: this.state.image }}
                />)}
              <Text style={styles.greyText}>
                {this.timestamp(this.props.data.timestamp)}
              </Text>
            </View>
          </>
        ) : (
          <>
            <View>
              <TouchableOpacity // Navigates to user profile when clicked
                onPress={() => {
                  this.props.navigation.push('SingleUser', this.props.item.user)
                }}
              >
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  {this.state.statusCode == 200 ? ( // eslint-disable-line eqeqeq
                    <>
                    <Image
                      style={styles.userImage}
                      source={{ uri: this.state.img }}
                    />
                    </>
                    ):(
                      <>
                      <Image
                      style={styles.userImage}
                      source={require('../styles/nopicture.png')}
                    />
                    </>
                    )}
                  <View>
                    <Text style={styles.user}>
                      {`${this.props.item.user.given_name} ${this.props.item.user.family_name}`}
                    </Text>
                    <Text style={styles.greyText}>{this.props.item.user.email}</Text>
                  </View>
                </View>
              </TouchableOpacity>
              <TouchableOpacity // Navigates to single chit when clicked
                onPress={() => {
                  this.props.navigation.push('SingleChit', this.state)
                }}
              >
                <Text style={styles.chit_content}> {this.props.item.chit_content}
                </Text>

                {this.state.status == 200 && (
                  <Image
                    style={styles.feedImage}
                    source={{ uri: this.state.image }}
                  />)}
              </TouchableOpacity>
              <Text style={styles.greyText}>
                {this.timestamp(this.props.item.timestamp)}
              </Text>
            </View>
          </>
        )}
      </View>
    )
  }
}

import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'
import { styles } from '../styles/Styles'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Geocoder from 'react-native-geocoding'
Geocoder.init('AIzaSyBYVgQR7ivcJ-nIB8BWBf6rm8FV8tI_ZPY', { language: 'en' })

class SingleChit extends Component {
  constructor (props) {
    super(props)
    this.state = {
      data: this.props.route.params.data,
      status: this.props.route.params.status,
      image: this.props.route.params.image,
      statusCode: this.props.route.params.statusCode,
      img: this.props.route.params.img
    }
  }

  getLocationFromLatLng (location) {
    Geocoder.from(location.latitude, location.longitude)
      .then(json => {
        this.setState({
          location_data: json.results[0].address_components[0].short_name
        })
      })
      .catch(error => console.warn(error))
  }

  timestamp (time) {
    const date = new Date(time)
    const postDate = `${date.getHours()}:${date.getMinutes()}  ${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`
    return postDate.toString()
  }

  componentDidMount () {
    if (this.state.data.location) {
      this.getLocationFromLatLng(this.state.data.location)
    }
  }

  render () {
    return (
      <View style={styles.single_chit}>
        <TouchableOpacity // Navigates to user profile when clicked
          onPress={() => {
            this.props.navigation.navigate('SingleUser', this.state.data.user)
          }}
        >
          <View style={{ flexDirection: 'row' }}>
            {this.state.statusCode == 200 && ( // eslint-disable-line eqeqeq
              <Image
                style={styles.userImage}
                source={{ uri: this.state.img }}
              />)}
            <View>
              <Text style={styles.user}>
                {`${this.state.data.user.given_name} ${this.state.data.user.family_name}`}
              </Text>
              <Text style={styles.greyText}>{this.state.data.user.email}
              </Text>
            </View>
          </View>
        </TouchableOpacity>

        <Text style={styles.chit_content}>
          {this.state.data.chit_content}
        </Text>
        {this.state.status == 200 && ( // eslint-disable-line eqeqeq
          <Image
            style={styles.singleImage}
            source={{ uri: this.state.image }}
          />)}
        <Text style={styles.greyText}>
          {this.timestamp(this.state.data.timestamp)}
        </Text>
        {this.state.data.location && (
          <Text style={styles.greyText}>
            {`Location: ${this.state.location_data}`}
          </Text>)}
      </View>

    )
  }
}

export default SingleChit

/* global fetch */

import React, { Component } from 'react'
import { styles } from '../styles/Styles'
import { Text, Alert, View, TouchableOpacity } from 'react-native'
import { TextInput } from 'react-native-paper'
import AsyncStorage from '@react-native-community/async-storage'

class PostChit extends Component {
  constructor (props) {
    super(props)
    this.state = {
    }
  }

  async postChit (token, chitData) {
    const data = JSON.stringify({
      timestamp: Date.now(),
      chit_content: chitData
    })

    try {
      const response = await fetch('http://10.0.2.2:3333/api/v0.0.5/chits', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'X-Authorization': token
        },
        body: data
      })
      const responseJson = await response.json()

      if (this.props.route.params.imageUrl != null) {
        console.log('Adding picture')
        return fetch(`http://10.0.2.2:3333/api/v0.0.5/chits/${responseJson.chit_id}/photo`, {
          method: 'POST',
          headers: {
            'Content-Type': 'image/jpeg',
            'X-Authorization': token
          },
          body: this.props.route.params.imageUrl
        })
      }
      this.props.navigation.push('AllChits')
    } catch (error) {
      console.error('ERROR', error)
    }
  }

  async componentDidMount () {
    this.setState({ token: await AsyncStorage.getItem('userToken') })
  }

  render () {
    return (
      <View>
        <TextInput
          style={{ height: 250 }}
          maxLength={141}
          multiline
          onChangeText={text =>
            this.setState({
              isLoading: false,
              chit_content: text
            })}
          value={this.state.chit_content}
        />
        <TouchableOpacity
          style={styles.postChit}
          onPress={() => {
            if (this.state.chit_content == null) {
              Alert.alert('Your chit is empty please type something')
            } else {
              this.postChit(this.state.token, this.state.chit_content)
            }
          }}
        >
          <Text>Post Chit</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.profileButton}
          onPress={() => {
            if (this.state.chit_content == null) {
              Alert.alert(
                'You cannot save an empty chit, please type something'
              )
            } else {
              AsyncStorage.setItem('chit', this.state.chit_content)
              this.props.navigation.push('AllChits')
            }
          }}
        >
          <Text>Save</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.profileButton}
          onPress={() => {
            this.props.navigation.navigate('Capture', 'PostChit')
          }}
        >
          <Text>Add a picture</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

export default PostChit

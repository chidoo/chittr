/* global fetch */

import React, { Component } from 'react'
import Chit from '../component/Chit'
import { styles } from '../styles/Styles'
import {
  FlatList,
  View
} from 'react-native'

class AllChitsFeed extends Component {
  constructor (props) {
    super(props)
    this.state = {
    }
  }

  async getChits () {
    try {
      const response = await fetch(
        'http://10.0.2.2:3333/api/v0.0.5/chits?count=100'
      )
      const responseJson = await response.json()
      this.setState({
        response: responseJson
      })
    } catch (error) {
      console.error('ERROR OCCURED: ', error)
    }
  }

  componentDidMount () {
    this.getChits()
  }

  render () {
    return (
      <FlatList
        data={this.state.response}
        keyExtractor={(item, index) => {
          return index.toString()
        }}
        renderItem={({ item, index }) => (
          <View
            key={index.toString()} style={styles.chit}>
            <Chit item={item} navigation={this.props.navigation} />
          </View>
        )}
      />
    )
  }
}
export default AllChitsFeed

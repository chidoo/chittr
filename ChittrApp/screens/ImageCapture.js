import React, { Component } from 'react'
import { styles } from '../styles/Styles'
import { Text, Alert, View, TouchableOpacity } from 'react-native'
import { RNCamera } from 'react-native-camera'

class CaptureImage extends Component {
  constructor (props) {
    super(props)
    this.state = {
    }
  }

  render () {
    return (
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref
          }}
          style={styles.preview}
          captureAudio={false}
        />
        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity
            onPress={this.takePicture.bind(this)}
            style={styles.postChit}
          >
            <Text style={{ fontSize: 16 }}>
               CAPTURE
            </Text>
          </TouchableOpacity>
        </View>
      </View>

    )
  }

  async takePicture () {
    if (this.camera) {
      const options = { quality: 0.5, base64: true }
      const data = await this.camera.takePictureAsync(options)
      if (this.props.route.params == 'PostChit') {
        this.props.navigation.navigate('PostChit', { imageUrl: data })
      } else {
        return fetch('http://10.0.2.2:3333/api/v0.0.5/user/photo', {
          method: 'POST',
          headers: {
            'Content-Type': 'image/jpeg',
            'X-Authorization': this.props.route.params.token
          },
          body: data
        })
          .then((response) => {
            Alert.alert('Picture Added!!')
            this.props.navigation.push('Profile')
          })
          .catch((error) => {
            console.errror(error)
          })
      }
    }
  }
}

export default CaptureImage

/* global fetch */

import React, { Component } from 'react'
import { TextInput, View, Text, TouchableOpacity, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import { styles } from '../styles/Styles'

export default class SignUp extends Component {
  constructor (props) {
    super(props)
    this.state = {
      name: '',
      lastname: '',
      email: '',
      password: '',
    }
  }

  async signUp () {
    const data = JSON.stringify({
      given_name: this.state.name,
      family_name: this.state.lastname,
      email: this.state.email,
      password: this.state.password
    })

    try {
      const response = await fetch('http://10.0.2.2:3333/api/v0.0.5/user', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: data
      })
      
      const user = JSON.stringify({
        email: this.state.email,
        password: this.state.password
      })

      const login = await fetch('http://10.0.2.2:3333/api/v0.0.5/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: user
      })

      const responseJson = await login.json()

      console.log(responseJson)
      await AsyncStorage.setItem('userToken', responseJson.token)
      await AsyncStorage.setItem('userId', responseJson.id.toString())
      await AsyncStorage.setItem('userEmail', this.state.email)
      await AsyncStorage.setItem('userName', this.state.name)
      await AsyncStorage.setItem('userLastname', this.state.lastname)

      this.props.navigation.navigate('AllChits')
      console.log(`ACCOUNT CREATED ${responseJson.id}`)
    } catch (error) {
      Alert.alert('Failure creating an account please try again')
    }
  }

  render () {
    return (
      <View style={styles.chit}>
        <TextInput
          style={styles.textInput}
          placeholder='Name'
          placeholderTextColor='#003f5c'
          onChangeText={text => this.setState({ name: text })}
        />
        <TextInput
          style={styles.textInput}
          placeholder='Lastname'
          placeholderTextColor='#003f5c'
          onChangeText={text => this.setState({ lastname: text })}
        />
        <TextInput
          style={styles.textInput}
          placeholder='Email Address'
          placeholderTextColor='#003f5c'
          onChangeText={text => this.setState({ email: text })}
        />
        <TextInput
          secureTextEntry
          style={styles.textInput}
          placeholder='Password'
          placeholderTextColor='#003f5c'
          onChangeText={text => this.setState({ password: text })}
        />
        <TouchableOpacity
          style={styles.profileButton}
          onPress={() => {
            this.signUp()
          }}
        >
          <Text>Signup</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

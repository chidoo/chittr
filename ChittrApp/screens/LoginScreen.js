/* global fetch */
import React, { Component } from 'react'
import { TextInput, View, Text, TouchableOpacity, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import { styles } from '../styles/Styles'

export default class Login extends Component {
  constructor (props) {
    super(props)
    this.state = {
      items: this.props.route.params
    }
  }

  async login () {
    const data = JSON.stringify({
      email: this.state.email,
      password: this.state.password
    })

    try {
      const response = await fetch('http://10.0.2.2:3333/api/v0.0.5/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: data
      })
      const responseJson = await response.json()
      this.setState({
        user_id: responseJson.id,
        user_token: responseJson.token
      })
      await AsyncStorage.setItem('userToken', responseJson.token)
      await AsyncStorage.setItem('userId', responseJson.id.toString())
      console.log('SUCCESSFULLY LOGGED' + responseJson.id.toString())
      this.getUserProfile()
    } catch (error) {
      console.log(error)
      Alert.alert('Failure logging in please check your credentials')
    }
  }

  async getUserProfile () {
    try {
      const response = await fetch(
        'http://10.0.2.2:3333/api/v0.0.5/user/' + this.state.user_id
      )
      const userData = await response.json()
      await AsyncStorage.setItem('userEmail', userData.email)
      await AsyncStorage.setItem('userName', userData.given_name)
      await AsyncStorage.setItem('userLastname', userData.family_name)

      this.state.items.navigate('AllChits')
    } catch (error) {
      console.log('ERROR OCCURED: ', error)
    }
  }

  render () {
    return (
      <View style={styles.chit}>
        <TextInput
          style={styles.textInput}
          placeholder='Email Address'
          placeholderTextColor='#D3D3D3'
          onChangeText={text => this.setState({ email: text })}
        />
        <TextInput
          style={styles.textInput}
          secureTextEntry
          placeholder='Password'
          placeholderTextColor='#D3D3D3'
          onChangeText={text => this.setState({ password: text })}
        />
        <TouchableOpacity
          style={styles.profileButton}
          onPress={() => {
            this.login()
          }}
        >
          <Text>LOGIN</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

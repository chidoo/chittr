import React, { Component } from 'react'
import Chit from '../component/Chit'
import { styles } from '../styles/Styles'
import {
  FlatList,
  ActivityIndicator,
  Text,
  View,
  Image
} from 'react-native'
import { Button } from 'react-native-paper'
import AsyncStorage from '@react-native-community/async-storage'

class UserDetails extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isLoading: true
    }
  }

  async getUserProfile () {
    try {
      const response = await fetch(
        `http://10.0.2.2:3333/api/v0.0.5/user/${this.props.route.params.user_id}`
      )
      const responseJson = await response.json()
      var data = []
      data.push(responseJson)
      this.setState({
        isLoading: false,
        response: responseJson.recent_chits,
        userInfo: responseJson
      })
    } catch (error) {
      console.error('ERROR OCCURED: ', error)
    }
  }

  async getUserProfilePicture () { // User's profile pictures
    try {
      const response = await fetch(
        `http://10.0.2.2:3333/api/v0.0.5/user/${this.props.route.params.user_id}/photo`
      )
      this.setState({
        img: response.url,
        statusCode: response.status
      })
    } catch (error) {
      console.error('ERROR OCCURED: ', error)
    }
  }

  async followUser (token) {
    try {
      await fetch(`http://10.0.2.2:3333/api/v0.0.5/user/${this.props.route.params.user_id}/follow`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'X-Authorization': token
        }
      })
      this.getUserFollowers()
    } catch (error) {
      console.error('ERROR OCCURED:', error)
    }
  }

  async unfollowUser (token) {
    try {
      await fetch(`http://10.0.2.2:3333/api/v0.0.5/user/${this.props.route.params.user_id}/follow`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          'X-Authorization': token
        }
      })
      this.getUserFollowers()
    } catch (error) {
      console.error('ERROR OCCURED:', error)
    }
  }

  async getUserFollowers () {
    try {
      const data = await fetch(
        `http://10.0.2.2:3333/api/v0.0.5/user/${this.props.route.params.user_id}/followers`
      )
      const responseJson = await data.json()
      this.setState({
        followers: responseJson
      })
    } catch (error) {
      console.error('ERROR OCCURED:', error)
    }
  }

  async getUserFollowing () {
    try {
      const data = await fetch(
        `http://10.0.2.2:3333/api/v0.0.5/user/${this.props.route.params.user_id}/following`
      )
      const responseJson = await data.json()
      this.setState({
        following: responseJson
      })
    } catch (error) {
      console.error('ERROR OCCURED: ', error)
    }
  }

  async componentDidMount () {
    this.setState({ token: await AsyncStorage.getItem('userToken') })
    this.setState({ id: await AsyncStorage.getItem('userId') })
    this.getUserFollowing()
    this.getUserFollowers()
    this.getUserProfilePicture()
    this.getUserProfile()
  }

  render () {
    if (this.state.isLoading) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size='large' color='#0c9' />
        </View>
      )
    }
    return (
      <View>
        <View style={styles.single_chit}>
          {this.state.statusCode == 200 && (
            <Image
              style={styles.userImage}
              source={{ uri: this.state.img }}
            />)}
          <Text style={styles.user}>{`${this.state.userInfo.given_name} ${this.state.userInfo.family_name}`}
          </Text>
          <Text style={styles.greyText}>{this.state.userInfo.email}</Text>
          <Text
            style={
              styles.user_content
            }
          >{`Followers: ${this.state.followers.length} Following: ${this.state.following.length}`}
          </Text>
          {this.props.route.params.user_id != this.state.id && this.state.token != null && (
            !this.state.followers.some(follower => follower.user_id == this.state.id) ? (
              <Button onPress={() => this.followUser(this.state.token)}>
                Follow
              </Button>
            ) : (
              <Button onPress={() => this.unfollowUser(this.state.token)}>
                Following
              </Button>
            ))}
        </View>
        <FlatList
          data={this.state.response}
          keyExtractor={(item, index) => {
            return index.toString()
          }}
          renderItem={({ item, index }) => (
            <View key={index.toString()} style={styles.chit}>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                {this.state.statusCode == 200 ? ( // eslint-disable-line eqeqeq
                  <>
                    <Image
                      style={styles.userImage}
                      source={{ uri: this.state.img }}
                    />
                  </>
                ) : (
                  <>
                    <Image
                      style={styles.userImage}
                      source={require('../styles/nopicture.png')}
                    />
                  </>
                )}
                <View>
                  <Text style={styles.user}>
                    {`${this.state.userInfo.given_name} ${this.state.userInfo.family_name}`}
                  </Text>
                  <Text style={styles.greyText}>{this.state.userInfo.email}</Text>
                </View>
              </View>
              <Chit data={item} navigation={this.props.navigation} />
            </View>

          )}
        />
      </View>
    )
  }
}
export default UserDetails

import React, { Component } from 'react'
import { styles } from '../styles/Styles'
import {
  TextInput,
  View,
  Alert,
  Image,
  Text,
  ActivityIndicator
} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler'

class Profile extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isLoading: true
    }
  }

  async updateAccount () {
    const data = JSON.stringify(
      this.state.updateItem
    )

    try {
      await fetch(`http://10.0.2.2:3333/api/v0.0.5/user/${this.state.id}`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'X-Authorization': this.state.token
        },
        body: data
      })
      console.log('SUCCESSFULLY UPDATED')
      this.setState({ updated: true })
    } catch (error) {
      console.error(error)
      Alert.alert('Failure updating your account')
    }
  }

  getUserProfilePicture (userId) {
    return fetch(`http://10.0.2.2:3333/api/v0.0.5/user/${userId}/photo`)
      .then(response => {
        this.setState({
          img: response.url,
          statusCode: response.status
        })
        this.getUserFollowers()
      })
      .catch(error => {
        console.error('ERROR OCCURED: ', error)
      })
  }

  async getUserFollowers () {
    try {
      const data = await fetch(
        `http://10.0.2.2:3333/api/v0.0.5/user/${this.state.id}/followers`
      )
      const responseJson = await data.json()
      this.setState({
        followers: responseJson
      })
      this.getUserFollowing()
    } catch (error) {
      console.error('ERROR OCCURED:', error)
    }
  }

  async getUserFollowing () {
    try {
      const data = await fetch(
        `http://10.0.2.2:3333/api/v0.0.5/user/${this.state.id}/following`
      )
      const responseJson = await data.json()
      this.setState({
        following: responseJson,
        isLoading: false
      })
    } catch (error) {
      console.error('ERROR OCCURED: ', error)
    }
  }

  async componentDidMount () {
    this.setState({
      token: await AsyncStorage.getItem('userToken'),
      id: await AsyncStorage.getItem('userId'),
      email: await AsyncStorage.getItem('userEmail'),
      name: await AsyncStorage.getItem('userName'),
      lastname: await AsyncStorage.getItem('userLastname')
    })
    this.getUserProfilePicture(this.state.id)
  }

  render () {
    if (this.state.isLoading) {
      return (
        <View style={styles.loader}>
          <ActivityIndicator size='large' color='#0c9' />
        </View>
      )
    }
    return (
      <ScrollView>
        <View>
          <View style={styles.single_chit}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('Capture', { token: this.state.token })
              }}
            >
              <View>
                {this.state.statusCode == 200 ? (
                  <>
                    <Image
                      style={styles.profileImage}
                      source={{ uri: this.state.img }}
                    />
                  </>
                ) : (
                  <>
                    <Image
                      style={styles.profileImage}
                      source={require('../styles/nopicture.png')}
                    />
                  </>
                )}
              </View>
            </TouchableOpacity>
            <Text
              style={[
                styles.user_content,
                { textAlign: 'center' }
              ]}
            >{`Followers: ${this.state.followers.length} Following: ${this.state.following.length}`}
            </Text>
          </View>

          <View style={[styles.rowAlign, styles.chit]}>
            <TextInput
              style={[styles.textInput, styles.profileInputBoxes]}
              onChangeText={text => this.setState({ name: text, updateItem: { given_name: text } })}
              value={this.state.name}
            />
            <TouchableOpacity
              style={styles.profileButton}
              onPress={() => {
                AsyncStorage.setItem('userName', this.state.name)
                this.updateAccount()
              }}
            >
              <Text>Update Name </Text>
            </TouchableOpacity>
          </View>

          <View style={[styles.rowAlign, styles.chit]}>
            <TextInput
              style={[styles.textInput, styles.profileInputBoxes]}
              onChangeText={text => this.setState({ lastname: text, updateItem: { family_name: text } })}
              value={this.state.lastname}
            />
            <TouchableOpacity
              style={styles.profileButton}
              onPress={() => {
                AsyncStorage.setItem('userLastname', this.state.lastname)
                this.updateAccount()
              }}
            ><Text>Update Lastname</Text>
            </TouchableOpacity>
          </View>

          <View style={[styles.rowAlign, styles.chit]}>
            <TextInput
              style={[styles.textInput, styles.profileInputBoxes]}
              onChangeText={text => this.setState({ email: text, updateItem: { email: text } })}
              value={this.state.email}
            />
            <TouchableOpacity
              style={styles.profileButton}
              onPress={() => {
                AsyncStorage.setItem('userEmail', this.state.email)
                this.updateAccount()
              }}
            ><Text>Update Email </Text>
            </TouchableOpacity>
          </View>

          <View style={[styles.rowAlign, styles.chit]}>
            <TextInput
              style={[styles.textInput, styles.profileInputBoxes]}
              secureTextEntry
              placeholder='Change your password'
              placeholderTextColor='#D3D3D3'
              onChangeText={text => this.setState({ password: text, updateItem: { password: text } })}
              value={this.state.password}
            />
            <TouchableOpacity
              style={styles.profileButton}
              onPress={() => {
                this.updateAccount()
              }}
            ><Text>Update Password</Text>
            </TouchableOpacity>
          </View>

        </View>
      </ScrollView>
    )
  }
}

export default Profile

/* global fetch */

import { SearchBar } from 'react-native-elements'
import React, { Component } from 'react'
import { View, FlatList } from 'react-native'
import UserList from '../component/UserSearch'
import { styles } from '../styles/Styles'

export default class SearchUsers extends Component {
  constructor (props) {
    super(props)
    this.state = {
      searching: false,
      search: '',
      location_data: '',
      chit_id: '',
      data: this.props.item
    }
  }

  getUsers (queryParam) {
    return fetch(`http://10.0.2.2:3333/api/v0.0.5/search_user?q=${queryParam}`)
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          searching: true,
          response: responseJson
        })
      })
      .catch(error => {
        console.error('ERROR OCCURED: ', error)
      })
  }

  render () {
    return (
      <View>
        <SearchBar
          searchIcon={false}
          placeholder='Search Chittr'
          onChangeText={text => {
            this.setState({ // eslint-disable-line no-unused-expressions
              search: text
            })
            this.getUsers(text)
          }}
          value={this.state.search}
        />
        {this.state.searching && (
          <View>
            <FlatList
              data={this.state.response}
              keyExtractor={(item, index) => {
                return index.toString()
              }}
              renderItem={({ item, index }) => (
                <View key={index.toString()} style={styles.chit}>
                  <UserList item={item} navigation={this.props.navigation} />
                </View>
              )}
            />
          </View>
        )}
      </View>
    )
  }
}

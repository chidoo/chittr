/* global fetch */
import React, { Component } from 'react'
import { styles } from '../styles/Styles'
import { Text, TextInput, View, TouchableOpacity, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'

class SavedChits extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isLoading: true,
      token: this.props.route.params
    }
  }

  async postChit (token, chitData) {
    const data = JSON.stringify({
      timestamp: Date.now(),
      chit_content: chitData
    })

    try {
      await fetch('http://10.0.2.2:3333/api/v0.0.5/chits', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'X-Authorization': token
        },
        body: data
      })

      this.props.navigation.push('AllChits')
      AsyncStorage.removeItem('chit')
    } catch (error) {
      console.error('ERROR', error)
    }
  }

  async getSavedChit () {
    this.setState({ savedChit: await AsyncStorage.getItem('chit') })
  }

  componentDidMount () {
    this.getSavedChit()
  }

  render () {
    if (this.state.savedChit == null) {
      return (
        <View style={styles.chit}>
          <Text>You do not have any saved chits</Text>
        </View>
      )
    }
    return (
      <View style={styles.chit}>
        <TextInput
          style={styles.textInput}
          onChangeText={text => this.setState({ savedChit: text })}
          value={this.state.savedChit}
        />
        <TouchableOpacity
          style={styles.postChit}
          onPress={() => {
            this.postChit(this.state.token, this.state.savedChit)
          }}
        >
          <Text>Post Chit</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.deleteButton}
          onPress={() => {
            AsyncStorage.removeItem('chit')
            Alert.alert('Your chit has been deleted')
          }}
        >
          <Text>Delete Chit</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.profileButton}
          onPress={() => {
            AsyncStorage.setItem('chit', this.state.savedChit)
            Alert.alert('Your changes have been saved')
          }}
        >
          <Text>Save Chit Chnages</Text>
        </TouchableOpacity>
      </View>
    )
  }
}
export default SavedChits
